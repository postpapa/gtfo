GTFO checks the charactername for unwanted words, you must determinate those words in your GameUserSettings.ini. 

GTFO destroys the character and kicks them from the server with a warning message letting them know that they can create a new one with a more appropriated name.
 
example: 

you set "butterfly" as whitelist

you set "butt" as unwanted name

a player named his char "superBuTthead" > he gets killed and kicked

a player named his char "ButterflyFan" > is allowed and can play

he gets killed and kicked

GameUserSettings:

[GTFO]

Whitelist=butterfly

NamesToKick=butt,123,rainbow,unicorn

KickByPlayerName=true/false

KickBySteamName=true/false

EnableLog=true/false

Whitelist checks ingame charactername and steamname (only the steam name the player had on first connect to server) for a allowed name

KickByPlayerName checks for the ingame charactername

KickBySteamName checks for the steamname (only the steam name the player had on first connect to server)

EnableLog writes the SteamID, charactername and Steamname into the ShooterGameLog of the server.

'NameFilter_BP' is a created 'Actor_BP' linked in the 'PrimalGameData_NameFilter' as 'Server Extra World Singleton Actor Classes'